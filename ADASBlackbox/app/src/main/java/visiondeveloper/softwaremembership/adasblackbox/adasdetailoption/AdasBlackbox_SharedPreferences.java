package visiondeveloper.softwaremembership.adasblackbox.adasdetailoption;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by SeungKyu-Lim on 2015-09-18.
 */
public class AdasBlackbox_SharedPreferences extends AdasBlackbox_SharedPreferenceKey {
    private SharedPreferences adasSharedPreferences;

    private String TAG = "AdasBlackbox_SharedPreferences";

    private Context mContext;

    AdasBlackbox_SharedPreferences(Context context) {
        mContext = context;
        adasSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);

    }

    // setter 함수는 하나로 관리된다.
    public void SetDetailOption(String key, Object value) {
        SharedPreferences.Editor editor = adasSharedPreferences.edit();
        if (key.equals(NORMALOPTION_MAXIMUMSTORAGE)) {
            editor.putInt(key, (int) value);
        } else if (key.equals(RECORDOPTION_BACKGROUND_RECORD)) {
            editor.putBoolean(key, (boolean) value);
        } else if (key.equals(RECORDOPTION_RECORD_TIME)) {
            editor.putInt(key, (int) value);
        } else if (key.equals(RECORDOPTION_CRASH_RECORD_TIME)) {
            editor.putInt(key, (int) value);
        } else if (key.equals(ADASOPTION_SIGNAL_ALERT)) {
            editor.putBoolean(key, (boolean) value);
        } else if (key.equals(SENSOROPTION_ENABLE_SENSOR)) {
            editor.putBoolean(key, (boolean) value);
        } else if (key.equals(SENSOROPTION_SENSITIVITY_LEVEL)) {
            editor.putInt(key, (int) value);
        } else if (key.equals(EMERGENCYOPTION_CALLNUMBER)) {
            editor.putString(key, value.toString());
        } else if (key.equals(EMERGENCYOPTION_DIRECTCALL)) {
            editor.putBoolean(key, (boolean) value);
        } else {
            Log.e(TAG, "SetDetailOption is not valid key");
            return;
        }
        editor.commit();
    }

    // getter 함수
    //최대 비디오 용량
    public int GetVideoStorage() {
        Log.d(TAG,"GetVideoStorage");
        Log.d(TAG,"error : "+adasSharedPreferences.getInt(NORMALOPTION_MAXIMUMSTORAGE, NORMALOPTION_DEFAULT_MAXIMUMSTORAGE));
        return adasSharedPreferences.getInt(NORMALOPTION_MAXIMUMSTORAGE, NORMALOPTION_DEFAULT_MAXIMUMSTORAGE);
    }

    // 녹화설정
    public boolean GetEnableRecordEnabled() {
        return adasSharedPreferences.getBoolean(RECORDOPTION_BACKGROUND_RECORD, RECORDOPTION_DEAFULT_BACKGROUND_RECORD);
    }

    public int GetRecordTime() {
        return adasSharedPreferences.getInt(RECORDOPTION_RECORD_TIME, RECORDOPTION_DEFAULT_RECORD_TIME);
    }

    public int GetCrashRecordTime() {
        return adasSharedPreferences.getInt(RECORDOPTION_CRASH_RECORD_TIME, RECORDOPTION_DEFAULT_CRASH_RECORD_TIME);
    }

    //ADAS 기능설정
    public boolean GetSignalAlertEnabled() {
        return adasSharedPreferences.getBoolean(ADASOPTION_SIGNAL_ALERT, ADASOPTION_DEFULT_SIGNAL_ALERT);
    }

    // 센서설정
    public boolean GetSensorEnabled()
    {
        return adasSharedPreferences.getBoolean(SENSOROPTION_ENABLE_SENSOR, SENSOROPTION_DEFAULT_ENABLE_SENSOR);
    }
    public int GetSensorSensitivityLevel()
    {
        return adasSharedPreferences.getInt(SENSOROPTION_SENSITIVITY_LEVEL, SENSOROPTION_DEFAULT_SENSITIVITY_LEVEL);
    }


    //긴급 연락처 설정
    public String GetEmergencyPhoneNumber() {
        return adasSharedPreferences.getString(EMERGENCYOPTION_CALLNUMBER, EMERGENCYOPTION_DEFAULT_CALLNUMBER);
    }

    public boolean GetEmergecyDirectCallEnabled() {
        return adasSharedPreferences.getBoolean(EMERGENCYOPTION_DIRECTCALL, EMERGENCYOPTION_DEFAULT_DIRECTCALL);
    }
}
