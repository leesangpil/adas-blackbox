package visiondeveloper.softwaremembership.adasblackbox.adasdetailoption;

/**
 * Created by SeungKyu-Lim on 2015-09-18.
 */
public class AdasBlackbox_SharedPreferenceKey {
    /*
    *  ADASBlackbox의 세부 설정을 이용하기 위한 Key값들입니다.
    *  옵션값을 수정하거나 얻을 때 일관된 Key값을 유지하기 위해 다음과 같은 클래스를 참조해 값을 얻습니다.
     */
    //일반설정
    static String NORMALOPTION_MAXIMUMSTORAGE = "NORMALOPTION_MAXIMUMSTORAGE";
    static int NORMALOPTION_DEFAULT_MAXIMUMSTORAGE = 5;

    //녹화설정
    static String RECORDOPTION_BACKGROUND_RECORD = "RECORDOPTION_BACKGROUND_RECORD";
    static String RECORDOPTION_RECORD_TIME = "RECORDOPTION_RECORD_TIME";
    static String RECORDOPTION_CRASH_RECORD_TIME = "RECORDOPTION_CRASH_RECORD_TIME";
    static boolean RECORDOPTION_DEAFULT_BACKGROUND_RECORD = true;
    static int RECORDOPTION_DEFAULT_RECORD_TIME = 5;
    static int RECORDOPTION_DEFAULT_CRASH_RECORD_TIME = 1;

    // ADAS기능설정
    static String ADASOPTION_SIGNAL_ALERT = "ADASOPTION_SIGNAL_ALERT";
    static boolean ADASOPTION_DEFULT_SIGNAL_ALERT = true;

    // 센서설정
    static String SENSOROPTION_ENABLE_SENSOR = "SENSOROPTION_ENABLE_SENSOR";
    static String SENSOROPTION_SENSITIVITY_LEVEL = "SENSOROPTION_SENSITIVITY_SENSOR";
    static boolean SENSOROPTION_DEFAULT_ENABLE_SENSOR = true;
    static int SENSOROPTION_DEFAULT_SENSITIVITY_LEVEL = 2;

    // 긴급 연락 설정
    static String EMERGENCYOPTION_CALLNUMBER = "EMERGENCYOPTION_CALLNUMBER";
    static String EMERGENCYOPTION_DIRECTCALL = "EMERGENCYOPTION_DIRECTCALL";
    static String EMERGENCYOPTION_DEFAULT_CALLNUMBER = "119";
    static boolean EMERGENCYOPTION_DEFAULT_DIRECTCALL = false;
}
