package visiondeveloper.softwaremembership.adasblackbox.adasdetailoption;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import visiondeveloper.softwaremembership.adasblackbox.R;

public class AdasBlackbox_DetailOptionActivity extends Activity {

    TextView tvVideoStorage;
    CheckBox cbRecordEnabled;
    TextView tvRecordTime;
    TextView tvCrashRecordTime;
    CheckBox cbSignalAlertEnabled;
    CheckBox cbSensorEnabled;
    TextView tvSensorSensitivityLevel;
    TextView tvEmergencyPhoneNumber;
    CheckBox cbEmergencyDirectCallEnabled;

    AdasBlackbox_SharedPreferences adasBlackbox_sharedPreferences = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adas_blackbox_detailoption);
        Init(); // process the findviewbyid and fill default value
    }

    void Init() {
        FindViewById();
        DefaultValueLoad();
    }

    void DefaultValueLoad() {
        if (adasBlackbox_sharedPreferences == null)
            adasBlackbox_sharedPreferences = new AdasBlackbox_SharedPreferences(this);
        tvVideoStorage.setText("" + adasBlackbox_sharedPreferences.GetVideoStorage());
        cbRecordEnabled.setChecked(adasBlackbox_sharedPreferences.GetEnableRecordEnabled());
        tvRecordTime.setText("" + adasBlackbox_sharedPreferences.GetRecordTime());
        tvCrashRecordTime.setText("" + adasBlackbox_sharedPreferences.GetCrashRecordTime());
        cbSignalAlertEnabled.setChecked(adasBlackbox_sharedPreferences.GetSignalAlertEnabled());
        cbSensorEnabled.setChecked(adasBlackbox_sharedPreferences.GetSensorEnabled());
        tvSensorSensitivityLevel.setText("" + adasBlackbox_sharedPreferences.GetSensorSensitivityLevel());
        tvEmergencyPhoneNumber.setText("" + adasBlackbox_sharedPreferences.GetEmergencyPhoneNumber());
        cbEmergencyDirectCallEnabled.setChecked(adasBlackbox_sharedPreferences.GetEmergecyDirectCallEnabled());
    }


    void FindViewById() {
        tvVideoStorage = (TextView) findViewById(R.id.detailoption_maximumstorage);
        cbRecordEnabled = (CheckBox) findViewById(R.id.detailoption_backgroud_record_enalbe);
        tvRecordTime = (TextView) findViewById(R.id.detailoption_recordtime);
        tvCrashRecordTime = (TextView) findViewById(R.id.detailoption_crash_recordtime);
        cbSignalAlertEnabled = (CheckBox) findViewById(R.id.detailoption_signal_alert);
        cbSensorEnabled = (CheckBox) findViewById(R.id.detailoption_sensor_enabled);
        tvSensorSensitivityLevel = (TextView) findViewById(R.id.detailoption_sensor_sensitivy);
        tvEmergencyPhoneNumber = (TextView) findViewById(R.id.detailoption_emergency_phonenumber);
        cbEmergencyDirectCallEnabled = (CheckBox) findViewById(R.id.detailoption_emergency_direct_call);


    }

    void SaveChangedOption()
    {
        adasBlackbox_sharedPreferences.SetDetailOption(AdasBlackbox_SharedPreferenceKey.NORMALOPTION_MAXIMUMSTORAGE, Integer.parseInt(tvVideoStorage.getText().toString()));
        adasBlackbox_sharedPreferences.SetDetailOption(AdasBlackbox_SharedPreferenceKey.RECORDOPTION_BACKGROUND_RECORD, cbRecordEnabled.isChecked());
        adasBlackbox_sharedPreferences.SetDetailOption(AdasBlackbox_SharedPreferenceKey.RECORDOPTION_RECORD_TIME, Integer.parseInt(tvRecordTime.getText().toString()));
        adasBlackbox_sharedPreferences.SetDetailOption(AdasBlackbox_SharedPreferenceKey.RECORDOPTION_CRASH_RECORD_TIME, Integer.parseInt(tvCrashRecordTime.getText().toString()));
        adasBlackbox_sharedPreferences.SetDetailOption(AdasBlackbox_SharedPreferenceKey.ADASOPTION_SIGNAL_ALERT, cbSignalAlertEnabled.isChecked());
        adasBlackbox_sharedPreferences.SetDetailOption(AdasBlackbox_SharedPreferenceKey.SENSOROPTION_ENABLE_SENSOR, cbSensorEnabled.isChecked());
        adasBlackbox_sharedPreferences.SetDetailOption(AdasBlackbox_SharedPreferenceKey.SENSOROPTION_SENSITIVITY_LEVEL, Integer.parseInt(tvSensorSensitivityLevel.getText().toString()));
        adasBlackbox_sharedPreferences.SetDetailOption(AdasBlackbox_SharedPreferenceKey.EMERGENCYOPTION_CALLNUMBER, tvEmergencyPhoneNumber.getText().toString());
        adasBlackbox_sharedPreferences.SetDetailOption(AdasBlackbox_SharedPreferenceKey.EMERGENCYOPTION_DIRECTCALL, cbEmergencyDirectCallEnabled.isChecked());
    }

    // it is must to refactoring by class.
    Dialog dialog;
    private AlertDialog SaveDialog() {
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle("잠시만요");
        ab.setMessage("변경사항을 저장하실래요?");
        ab.setCancelable(false);

        ab.setPositiveButton("저장", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                dialog.dismiss();
                SaveChangedOption();
                finish();
            }
        });

        ab.setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                dialog.dismiss();
                finish();
            }
        });

        return ab.create();
    }

    @Override
    public void onBackPressed() {
        dialog = SaveDialog();
        dialog.show();
    }

}
